package domain;

import java.awt.Image;
import java.awt.Toolkit;
import java.time.*;
import java.time.temporal.ChronoUnit;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.time.format.DateTimeFormatter;

/**
 *
 * @author Erick Leser Gonzalez
 */
public class CalculaTiempo extends javax.swing.JFrame {

    private void ponerFechaYHora() {
        Date date = new Date();
        SimpleDateFormat hourdateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:00");
        mostrartiempo.setText(hourdateFormat.format(date));
    }
     private void transparente(){
    regresar.setOpaque(false);
    regresar.setContentAreaFilled(false);
    regresar.setBorderPainted(false);
    
    cerrar.setOpaque(false);
    cerrar.setContentAreaFilled(false);
    cerrar.setBorderPainted(false);
    }
    private void tiempoDeNovios(){
        LocalDate fecha = LocalDate.now();
        LocalTime ahora = LocalTime.now();
        LocalDateTime horaActual = LocalDateTime.of(fecha, ahora);
        
        LocalDateTime fechaPuesta = LocalDateTime.of(2021, Month.FEBRUARY, 8, 17, 55, 00);
        
        long minutosFaltantes = ChronoUnit.MINUTES.between(fechaPuesta, horaActual);
       
        long horasFaltantes = ChronoUnit.HOURS.between(fechaPuesta, horaActual);
        long diasFaltantes = ChronoUnit.DAYS.between(fechaPuesta, horaActual);
        long semanasFaltantes = ChronoUnit.WEEKS.between(fechaPuesta, horaActual);
        long mesesFaltantes = ChronoUnit.MONTHS.between(fechaPuesta, horaActual);
        long aniosFaltantes = ChronoUnit.YEARS.between(fechaPuesta, horaActual);

        if (aniosFaltantes >= 1) {
            horaActual = horaActual.minusYears(aniosFaltantes);
        }
        mesesFaltantes = ChronoUnit.MONTHS.between(fechaPuesta, horaActual);
        if (mesesFaltantes >= 1) {
            horaActual = horaActual.minusMonths(mesesFaltantes);

        }
        semanasFaltantes = ChronoUnit.WEEKS.between(fechaPuesta, horaActual);
        if (semanasFaltantes >= 1) {
            horaActual = horaActual.minusWeeks(semanasFaltantes);

        }
        diasFaltantes = ChronoUnit.DAYS.between(fechaPuesta, horaActual);
        if (diasFaltantes >= 1) {
            horaActual = horaActual.minusDays(diasFaltantes);

        }
        horasFaltantes = ChronoUnit.HOURS.between(fechaPuesta, horaActual);
        if (horasFaltantes >= 1) {
            horaActual = horaActual.minusHours(horasFaltantes);

        }
        minutosFaltantes = ChronoUnit.MINUTES.between(fechaPuesta, horaActual);
        if (minutosFaltantes >= 1300) {
            minutosFaltantes = minutosFaltantes - 1440;
        }

        String minutosString = String.valueOf(minutosFaltantes);
        String horasString = String.valueOf(horasFaltantes);
        String diasString = String.valueOf(diasFaltantes);
        String semanasString = String.valueOf(semanasFaltantes);
        String mesesString = String.valueOf(mesesFaltantes);
        String aniosString = String.valueOf(aniosFaltantes);

        if (aniosFaltantes >= 1) {
            horaaaaas.setText("Llevamos " + aniosString + " año(s), " + mesesString + " mes(es), " + semanasString + " semana(s), " + diasString + " dia(s), " + horasString + " hora(s) y " + minutosString + " minuto(s)");
        } else if (aniosFaltantes == 0 && mesesFaltantes >= 1) {
            horaaaaas.setText("Llevamos " + mesesString + " mes(es), " + semanasString + " semana(s), " + diasString + " dia(s), " + horasString + " hora(s) y " + minutosString + " minuto(s)");
        } else if (mesesFaltantes == 0 && semanasFaltantes >= 1) {
            horaaaaas.setText("Llevamos " + semanasString + " semana(s), " + diasString + " dia(s), " + horasString + " hora(s) y " + minutosString + " minuto(s)");
        } else if (semanasFaltantes == 0 && diasFaltantes >= 1) {
            horaaaaas.setText("Llevamos " + diasString + " dia(s), " + horasString + " hora(s) y " + minutosFaltantes + " minuto(s)");
        } else if (diasFaltantes == 0 && horasFaltantes >= 1) {
            horaaaaas.setText("Llevamos " + horasString + " hora(s) y " + minutosString + " minuto(s)");
        } else if (horasFaltantes == 0 && minutosFaltantes >= 1) {
            horaaaaas.setText("Llevamos " + minutosString + " minuto(s)");
        } 
    }
    
    private void calcularTiempo() {
        LocalDate hoy = LocalDate.now();
        LocalTime ahora = LocalTime.now();
        LocalDateTime horaActual = LocalDateTime.of(hoy, ahora);

        String tiempoPuesto = mostrartiempo.getText();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");
        LocalDateTime fechaPuesta = LocalDateTime.parse(tiempoPuesto, formatter);

        long minutosFaltantes = ChronoUnit.MINUTES.between(horaActual, fechaPuesta);
        long horasFaltantes = ChronoUnit.HOURS.between(horaActual, fechaPuesta);
        long diasFaltantes = ChronoUnit.DAYS.between(horaActual, fechaPuesta);
        long semanasFaltantes = ChronoUnit.WEEKS.between(horaActual, fechaPuesta);
        long mesesFaltantes = ChronoUnit.MONTHS.between(horaActual, fechaPuesta);
        long aniosFaltantes = ChronoUnit.YEARS.between(horaActual, fechaPuesta);

        aniosFaltantes = ChronoUnit.YEARS.between(horaActual, fechaPuesta);

        if (aniosFaltantes >= 1) {
            fechaPuesta = fechaPuesta.minusYears(aniosFaltantes);
        }
        mesesFaltantes = ChronoUnit.MONTHS.between(horaActual, fechaPuesta);
        if (mesesFaltantes >= 1) {
            fechaPuesta = fechaPuesta.minusMonths(mesesFaltantes);

        }
        semanasFaltantes = ChronoUnit.WEEKS.between(horaActual, fechaPuesta);
        if (semanasFaltantes >= 1) {
            fechaPuesta = fechaPuesta.minusWeeks(semanasFaltantes);

        }
        diasFaltantes = ChronoUnit.DAYS.between(horaActual, fechaPuesta);
        if (diasFaltantes >= 1) {
            fechaPuesta = fechaPuesta.minusDays(diasFaltantes);

        }
        horasFaltantes = ChronoUnit.HOURS.between(horaActual, fechaPuesta);
        if (horasFaltantes >= 1) {
            fechaPuesta = fechaPuesta.minusHours(horasFaltantes);

        }
        minutosFaltantes = ChronoUnit.MINUTES.between(horaActual, fechaPuesta);
        if (minutosFaltantes >= 1300) {
            minutosFaltantes = minutosFaltantes - 1440;
        }

        String minutosString = String.valueOf(minutosFaltantes);
        String horasString = String.valueOf(horasFaltantes);
        String diasString = String.valueOf(diasFaltantes);
        String semanasString = String.valueOf(semanasFaltantes);
        String mesesString = String.valueOf(mesesFaltantes);
        String aniosString = String.valueOf(aniosFaltantes);

        if (aniosFaltantes >= 1) {
            resultado.setText("Falta(n) " + aniosString + " año(s), " + mesesString + " mes(es), " + semanasString + " semana(s), " + diasString + " dia(s), " + horasString + " hora(s) y " + minutosString + " minuto(s)");
        } else if (aniosFaltantes == 0 && mesesFaltantes >= 1) {
            resultado.setText("Falta(n) " + mesesString + " mes(es), " + semanasString + " semana(s), " + diasString + " dia(s), " + horasString + " hora(s) y " + minutosString + " minuto(s)");
        } else if (mesesFaltantes == 0 && semanasFaltantes >= 1) {
            resultado.setText("Falta(n) " + semanasString + " semana(s), " + diasString + " dia(s), " + horasString + " hora(s) y " + minutosString + " minuto(s)");
        } else if (semanasFaltantes == 0 && diasFaltantes >= 1) {
            resultado.setText("Falta(n) " + diasString + " dia(s), " + horasString + " hora(s) y " + minutosFaltantes + " minuto(s)");
        } else if (diasFaltantes == 0 && horasFaltantes >= 1) {
            resultado.setText("Falta(n) " + horasString + " hora(s) y " + minutosString + " minuto(s)");
        } else if (horasFaltantes == 0 && minutosFaltantes >= 1) {
            resultado.setText("Falta(n) " + minutosString + " minuto(s)");
        } else {
            resultado.setText("No sea tonta, mija. Meta bien la fecha.");
        }

    }

    /**
     * Creates new form NewJFrame
     */
    public CalculaTiempo() {
        initComponents();
        transparente();
        
        this.setLocationRelativeTo(null);
        ponerFechaYHora();
        tiempoDeNovios();
    }

    public Image getIconImage() {
        Image retValue = Toolkit.getDefaultToolkit().getImage(ClassLoader.getSystemResource("imagenes/jocabed.jpg"));
        return retValue;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        regresar = new javax.swing.JButton();
        cerrar = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();
        mostrar = new javax.swing.JButton();
        resultado = new javax.swing.JLabel();
        horaaaaas = new javax.swing.JLabel();
        mostrartiempo = new javax.swing.JFormattedTextField();
        masmenos = new javax.swing.JLabel();
        imgregresar = new javax.swing.JLabel();
        imgregresa = new javax.swing.JLabel();
        Fondo = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setIconImage(getIconImage());
        setUndecorated(true);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        regresar.setFont(new java.awt.Font("OCR A Extended", 0, 18)); // NOI18N
        regresar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        regresar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                regresarActionPerformed(evt);
            }
        });
        getContentPane().add(regresar, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 50, 70, 70));

        cerrar.setBackground(new java.awt.Color(52, 152, 219));
        cerrar.setFont(new java.awt.Font("OCR A Extended", 0, 18)); // NOI18N
        cerrar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        cerrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cerrarActionPerformed(evt);
            }
        });
        getContentPane().add(cerrar, new org.netbeans.lib.awtextra.AbsoluteConstraints(1020, 50, 70, 70));

        jLabel3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/reloj-digital.png"))); // NOI18N
        getContentPane().add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(450, 50, 290, 280));

        mostrar.setBackground(new java.awt.Color(52, 152, 219));
        mostrar.setFont(new java.awt.Font("OCR A Extended", 0, 18)); // NOI18N
        mostrar.setText("Ingresa cuando nos veremos y luego presioname");
        mostrar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        mostrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mostrarActionPerformed(evt);
            }
        });
        getContentPane().add(mostrar, new org.netbeans.lib.awtextra.AbsoluteConstraints(310, 440, -1, -1));

        resultado.setFont(new java.awt.Font("OCR A Extended", 0, 23)); // NOI18N
        resultado.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        resultado.setText("You're beautiful");
        getContentPane().add(resultado, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 520, 1160, -1));

        horaaaaas.setFont(new java.awt.Font("OCR A Extended", 0, 24)); // NOI18N
        horaaaaas.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        horaaaaas.setText("You'll never see this whitout the code, but i loveu ");
        getContentPane().add(horaaaaas, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 580, 1150, 40));

        mostrartiempo.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.DateFormatter(new java.text.SimpleDateFormat("dd/MM/yyyy HH:mm:ss"))));
        mostrartiempo.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        mostrartiempo.setText("dd/MM/yyyy HH:mm:ss");
        mostrartiempo.setFont(new java.awt.Font("OCR A Extended", 0, 18)); // NOI18N
        mostrartiempo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mostrartiempoActionPerformed(evt);
            }
        });
        getContentPane().add(mostrartiempo, new org.netbeans.lib.awtextra.AbsoluteConstraints(420, 330, 320, 70));

        masmenos.setFont(new java.awt.Font("OCR A Extended", 0, 24)); // NOI18N
        masmenos.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        masmenos.setText("+-5 minutos xd");
        getContentPane().add(masmenos, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 620, 1160, 50));

        imgregresar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/cerrar-sesion.png"))); // NOI18N
        getContentPane().add(imgregresar, new org.netbeans.lib.awtextra.AbsoluteConstraints(1020, 50, -1, -1));

        imgregresa.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/repetir (1).png"))); // NOI18N
        getContentPane().add(imgregresa, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 50, -1, -1));

        Fondo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/degradado.png"))); // NOI18N
        Fondo.setMaximumSize(new java.awt.Dimension(1920, 720));
        Fondo.setMinimumSize(new java.awt.Dimension(1920, 720));
        Fondo.setPreferredSize(new java.awt.Dimension(1920, 720));
        getContentPane().add(Fondo, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1155, -1));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void regresarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_regresarActionPerformed
        Interface abrir = new Interface();
        abrir.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_regresarActionPerformed

    private void cerrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cerrarActionPerformed
        System.exit(0);
    }//GEN-LAST:event_cerrarActionPerformed

    private void mostrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mostrarActionPerformed
        calcularTiempo();
    }//GEN-LAST:event_mostrarActionPerformed

    private void mostrartiempoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mostrartiempoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_mostrartiempoActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(CalculaTiempo.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(CalculaTiempo.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(CalculaTiempo.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(CalculaTiempo.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new CalculaTiempo().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel Fondo;
    private javax.swing.JButton cerrar;
    private javax.swing.JLabel horaaaaas;
    private javax.swing.JLabel imgregresa;
    private javax.swing.JLabel imgregresar;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel masmenos;
    private javax.swing.JButton mostrar;
    private javax.swing.JFormattedTextField mostrartiempo;
    private javax.swing.JButton regresar;
    private javax.swing.JLabel resultado;
    // End of variables declaration//GEN-END:variables
}
